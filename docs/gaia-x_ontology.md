# Gaia-X Ontology

The Gaia-X Ontology is the technical embodiment of the Gaia-X Governance which allows to measure and objectivise services and dataset properties required for Gaia-X Participant to self-determine their level of technical, operational and legal autonomy.
The ontology is available via the [Gaia-X Registry](#gaia-x_registry.md) hosted by the [GXDCH](#GXDCH.md).

## References

- [Gaia-X Architecture Document](architecture_document.md) 23.0X- draft
