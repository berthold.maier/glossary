# Contract

A Contract is an agreement between a [Consumer](consumer.md) and a [Provider](provider.md), to allow and regulate the usage of one or more [Service Instances](service_instance.md). It is related to a specific version of a [Service](service.md), from which it derives the attributes of the Service Instances to be provisioned.
The Contract has a distinct lifecycle from the Service Offering and additional attributes and logic, which are out of scope of [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/).

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 4.7
