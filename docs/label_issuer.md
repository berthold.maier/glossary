# Label Issuer

Label Issuers are entities, defined by the [Gaia-X Association](gaia-x.md) to implement and issue a [Label](label.md). The implementation of a Label consists in the decomposition of all Label requirements into [Verifiable Credentials](verifiable_credential.md) that are then encoded in the [Compliance](gaia-x_trust_framework.md) and Labelling framework, so that they can be verified automatically where possible. The issuer of a Label can be Gaia-X or another Issuer verified and accepted by Gaia-X.

## References
- [Gaia-X Labelling Framework](https://gaia-x.eu/wp-content/uploads/files/2021-11/Gaia-X%20Labelling%20Framework_0.pdf)

