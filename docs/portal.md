# Portal

The Portals provide information on [Resources](resource.md) and [Service Offerings](service_offering.md) and interaction mechanisms for tasks related to their maintenance, including identity and access management. Each [ecosystem](ecosystem.md) can deploy its own Portal to support interaction with [Federation Services](federation_services.md).

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 7.8
