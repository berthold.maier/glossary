# Consumer Policy

 Consumer Policy describes a [Consumer](consumer.md)'s restrictions of a requested [Resource](resource.md). 

 ## References
 - [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 4.5 

## alias
- Search Policy
