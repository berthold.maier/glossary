# Self-Description

Gaia-X Self-Descriptions (SD) describe Entities from the Gaia-X Conceptual Model in a machine interpretable format. Self-Descriptions are [W3C Verifiable Presentations](https://www.w3.org/TR/vc-data-model/#presentations) in the [JSON-LD format](https://www.w3.org/TR/json-ld11/). Self-Descriptions comprise signed or unsigned arrays of one or more [Verifiable Credentials](https://www.w3.org/TR/vc-data-model/#credentials).

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 5.4

## alias
- The term is now being replaced by [Gaia-X_credentials](Gaia-X_credentials.md)