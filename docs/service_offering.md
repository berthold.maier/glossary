# Service Offering

A Service Offering is a set of [Resources](resource.md), which a [Provider](provider.md) aggregates and publishes as a single entry in a [Catalogue](catalogue.md). 

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 4.7
