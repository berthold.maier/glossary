# Policy

Statement of objectives, rules, practices, or regulations governing the activities of [Participants](participant.md) within Gaia-X. From a technical perspective Policies are statements, rules or assertions that specify the correct or expected behaviour of an [entity](entity.md).

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 4.5
