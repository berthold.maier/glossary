# Gaia-X Compliance Service

This service validates the shape, content and credentials of [Self Descriptions](self_description.md) and issues a [Verifiable Credential](verifiable_credential-md) attesting of the result. Required fields and consistency rules are defined in the [Gaia-X Trust Framework](gaia-x_trust_framework.md). Format of the input and output are defined in the [Identity, Credential and Access management document](https://docs.gaia-x.eu/technical-committee/identity-credential-access-management/latest/).

## References
- [Gaia-X Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/) 22.10 - 2.1.2

