# Data Exchange Component

The data exchange component is a service to negotiate (signaling layer) and exchange dataset (media layer).

## References
- [Gaia-X Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/) 22.10, 6.3
