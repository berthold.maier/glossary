# Gaia-X Technical Committee

The Technical Committee defines and implements the technological vision of Gaia-X. It plans, develops, and is accountable for the Gaia-X technology roadmap and its contributors. Furthermore, it communicates the Gaia-X technological vision and its related objectives to establish trust and credibility with members and third parties.

## References
- https://gaia-x.eu/who-we-are/association/
