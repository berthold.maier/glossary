# ecosystem (general term for autonomous independent ecosystems )

An ecosystem is an independant group of [Participants](participant.md) agreeing to a common set of rules to directly or indirectly consume, produce, or provide services such as data, storage, computing, network services, including combinations of them. Those Participants autonomously decide which information to share, with whom, and if they want to do it in a Gaia-X compliant way.

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 6.1

