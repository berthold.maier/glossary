# Trust Framework

It is a [Gaia-X deliverable](gaia-x_deliverables.md) containing the set of rules that define the minimum baseline to be part of the [Gaia-X Ecosystem](gaia-x_ecosystem.md). 
Those rules provide a common governance and the basic level of interoperability across individual ecosystems while letting the users in full control of their choices.

## References
- [Gaia-X Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/)
- [Gaia-X Framework](gaia-x_framework.md)

## alias
- [Gaia-X Trust Framework](gaia-x_trust_framework.md)
