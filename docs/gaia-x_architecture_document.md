# Gaia-X Architecture Document

The Gaia-X Architecture Document is a [Gaia-X deliverable](gaia-x_deliverable) that describes the top-level Gaia-X Architecture model. It focuses on conceptual modelling and key considerations of an operating model and is agnostic regarding technology and vendor.
It describes the concepts required to realize Gaia-X compliant data ecosystems and integrates the [Providers](provider.md), [Consumers](consumer.md), and [Services](service_offering.md) essential for their interaction.

## References
- [Gaia-X Framework](https://docs.gaia-x.eu/framework/)
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 2.2,2.3

## alias
- Architecture Document
