# Secure Digital Identity

Unique Identity with additional data for robustly trustworthy authentication of the [entity](entity.md) (i.e. with appropriate measures to prevent impersonation). 

## References
- - [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 7.5
