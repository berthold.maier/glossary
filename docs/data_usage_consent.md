# Data Usage Consent 

A legally binding agreement made by and between a [Data Producer](data_producer.md) and [Data Provider](data_provider.md) specifying the terms and conditions to the usage of the data. Data Usage Consent constitutes a legal usage consent for data which are subject to GDPR.

## References
- [GXFSv2 Data Exchange](https://gaia-x.gitlab.io/technical-committee/federation-services/data-exchange/) 22.10, 2.2 
