# Conformity Assessment

Conformity assessment is the demonstration that specified requirements relating to a product, process, service, person, system or body are fulfilled.

## References
- <https://www.iso.org/foreword-supplementary-information.html>
