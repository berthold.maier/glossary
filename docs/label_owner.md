# Label Owner

Label Owners are entities that decide to define a specific Label for their business. These can be Service Providers, Service
Users or Governmental Authorities, Standardisation Authorities, Trade Associations, Industrial Associations, etc. 
The decision to define a Gaia-X Label is made based on the potential use and benefit of it. So, a Banking Association can decide to define a Label
specific to Banks, to ensure all cloud adopted by their associates can fulfill specific requirements. In this example, the Banking
Association defines the name of the Label and specifies the requirements to be verified (e.g., Territorial Jurisdiction, service
location, required certifications, etc.). The Label Owner engages the Gaia-X Association to develop the Label and issue it and
must receive the agreement of Gaia-X formally.

## References
- [Gaia-X Labelling Framework](https://gaia-x.eu/wp-content/uploads/files/2021-11/Gaia-X%20Labelling%20Framework_0.pdf)
