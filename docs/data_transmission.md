# Data Transmission 

The term “Data Transmission” symbolizes a unique unit of data transmission, logged, treated in a coherent and reliable way independent of other transactions. It is materialized by a Signed [Data Product Self-Description](data_product_self-description.md).

## References
- [GXFSv2 Data Exchange](https://gaia-x.gitlab.io/technical-committee/federation-services/data-exchange/) 22.10, 2.2 
