# Gaia-X Label

Gaia-X Labels are logical groupings of composable service attributes which guarantee that [Service Offerings](service_offering.md) meet a specific set of respective requirements with regards to the main objectives: data protection, transparency, security, portability and flexibility, and European control.
Gaia-X Labels are not mandatory and can be extended to meet requirements that are specific to countries or vertical domains, with the authorization of the [Gaia-X Association](gaia-x_association.md).

## References
- [Gaia-X Labelling Framework](https://gaia-x.eu/wp-content/uploads/files/2021-11/Gaia-X%20Labelling%20Framework_0.pdf)
- [Gaia-X Policy Rules and Labelling Document](link), 22.11



