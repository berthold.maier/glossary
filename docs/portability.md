# Portability

Portability describes the ability to move data or applications between two different services at a low cost and with minimal disruption.

## References
- adapted from [ISO/IEC 19941:2017(en)](https://www.iso.org/standard/66639.html)
