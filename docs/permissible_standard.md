# Permissible Standard (ref. Policy Rules and Labelling Document)

In the context of the definition of Gaia-X Labelling Criteria (see [Policy Rules and Labelling Document](https://docs.gaia-x.eu/policy-rules-committee/policy-rules-labelling/latest/policy_rules_labeling_document/), Permissible Standards shall identify standards respectively requirements / controls within such standards, where implementation shall be considered a prima facie evidence of conformity with the related Gaia-X criterion.

## References
- [Gaia-X Policy Rules and Labelling Document](https://docs.gaia-x.eu/policy-rules-committee/policy-rules-labelling/latest/policy_rules_labeling_document/), 2.2.5
