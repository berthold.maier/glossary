# Customer Data

In the context of the [Gaia-X Policy Rules and Labelling Criteria Document](link), the term is used for all customer provided or generated data, both personal and non-personal data, as processed by a Provider. This is not about the data-about-the-Customer, which the Provider needs to
administer the service offering, to deploy, meter and bill the service to the Customer.

## References

Gaia-X Policy Rules and Labelling Criteria Document, Gaia-X Policy Rules and Labelling Criteria for Providers
