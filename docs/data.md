# Data

Any digital representation of acts, facts or information and any compilation of such acts, facts or information.

## References
- [GXFSv2 Data Exchange](https://gaia-x.gitlab.io/technical-committee/federation-services/data-exchange/) 22.10, 2.2 
