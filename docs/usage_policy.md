# Usage Policy

Usage Policies constrains the [Consumer](consumer.md)'s use of a [Resource](resource.md).

## References
- [Gaia-X Architecture Document](gaia-x_architecture_document.md) 22.10 - 4.5

## alias
- Provider Policy


