# Gaia-X Ecosystem

The Gaia-X Ecosystem is the virtual set of [Participants](participant.md), [Service Offerings](service_offering.md) and [Resources](resource.md) fulfilling the requirements of the Gaia-X Trust Framework.

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 3.5

