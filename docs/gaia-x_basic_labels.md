# Gaia-X Basic Labels

The Gaia-X Basic Labels are three clusters of compliance criteria defined in the [Gaia-X Labelling Framework](gaia-x_labelling_framework.md).
They correspond to: [Label Level 1](label_level_1.md), [Label Level 2](label_level_2.md),[Label Level 3](label_level_3.md), defined as follows:

▪ [Label Level 1](label_level_1.md) – Data protection, transparency, security, portability, and flexibility are guaranteed in line with the rules defined in the [Gaia-X Policy Rules Document](gaia-x_policy_rules_and_labelling_document.md) and the basic set of technical requirements derived from the [Gaia-X Architecture Document](gaia-x_architecture_document.md). For cybersecurity, with the minimum requirement being to meet ENISA’s European Cybersecurity Scheme - Basic Level.

▪ [Label Level 2](label_level_2.md) – This advanced Label Level 2 extends the basic requirements from Level 1 and reflects a higher level of security, transparency of applicable legal rules and potential dependencies. The option of a service location in Europe must be provided
to the consumer. Regarding cybersecurity, the minimum requirement will be to meet ENISA European Cybersecurity Scheme -
Substantial Level.

▪ [Label Level 3](label_level_3.md) – This level targets the highest standards for data protection, security, transparency, portability, and flexibility, as well as European control. It extends the requirements of Levels 1 and 2, with criteria that ensure immunity to non-European
access and a strong degree of control over vendor lock-in. A service location in Europe is mandatory. For cybersecurity, the
minimum requirement will be to meet ENISA’s European Cybersecurity Scheme - High Level. 

## References
- [Gaia-X Labelling Framework](https://gaia-x.eu/wp-content/uploads/files/2021-11/Gaia-X%20Labelling%20Framework_0.pdf)
