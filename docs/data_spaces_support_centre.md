# Data Spaces Support Centre (DSSC)

The combination of all activities and the output of the EU-funded project with the same
name. It is the virtual organisation that promotes and coordinates all relevant actions on sectoral data spaces. It provides
an overview of technologies, processes, standards, and tools that will support the deployment of common data spaces
and will allow the reuse of data across sectors. The Data Spaces Support Centre manages DSSC assets and develops and
executes strategies to provide continuity for the main assets also beyond the project funding

## References
- DSSC Glossary - [DSSC Starter Kit - Dec 2022](file:///C:/Users/giudi/OneDrive%20-%20GAIA-X,%20the%20European%20Association%20for%20Data%20and%20Cloud,%20AISBL/Desktop/Op%20Hand/Starterkit-Interim-Version-Release-19-Dec-2022.pdf)

