# Virtual Resource

A Virtual Resource represents static data in any form and necessary information such as dataset, configuration file, license, keypair, an AI model, neural network weights, etc.

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 4.3
