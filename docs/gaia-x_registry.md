# Gaia-X Registry

The Gaia-X Registry is a public distributed, non-repudiable, immutable, permissionless database with a decentralized infrastructure and the capacity to automate code execution.
This service provides a list of valid shapes, and valid and revoked public keys. The Gaia-X Registry will also be used as the seeding list for the network of catalogues. 

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 6.6.1
- [Gaia-X Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/) 22.10 - 2.1.2

## alias
- Verifiable Data Registry
