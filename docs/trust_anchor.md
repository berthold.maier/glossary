# Trust Anchor

Trust Anchors are entities endorsed by [Gaia-X](gaia-x.md). A Trust Anchor can be either the entity in direct control of the [Trusted Data Source](trusted_data_source.md) or a [Notary](notary.md).[Ecosystems](ecosystem.md) can select their own Trust Anchors, however, cross-ecosystem trust provided by Gaia-X requires the selected Trust Anchors to comply with the rules defined by the Gaia-X Association.
The list of valid Trust Anchors is stored in the [Gaia-X Registry](gaia_x_registry.md).

## References
- [Gaia-X Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/) 22.10 - 4
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 6.2



