# Data Product 

A collection of one or more [data](data.md) that are packaged by the [Data Provider](data_provider.md) and made ready for Data Exchange.

## References
- [GXFSv2 Data Exchange](https://gaia-x.gitlab.io/technical-committee/federation-services/data-exchange/) 22.10, 2.2 

## alias
- [Data Resource](data_resource.md)
