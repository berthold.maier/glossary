# Data Space

Decentralised, governed and standard-based structure to enable trustworthy data sharing between the
data space participants on a voluntary basis. Data spaces may be purpose- or sector-specific, or cross-sectoral. Common European data spaces are a subset of data spaces within the scope of EU policies.

## References
- DSSC Glossary - [DSSC Starter Kit - Dec 2022](file:///C:/Users/giudi/OneDrive%20-%20GAIA-X,%20the%20European%20Association%20for%20Data%20and%20Cloud,%20AISBL/Desktop/Op%20Hand/Starterkit-Interim-Version-Release-19-Dec-2022.pdf)
