# Infrastructure ecosystem

It refers to an [ecosystem](ecosystem.md) that has a focus on computing, storage and Interconnection elements.

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 7.1.3
