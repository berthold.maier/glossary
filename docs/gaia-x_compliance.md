# Gaia-X Compliance

The Gaia-X Compliance is split in two main subsystems:

- The [Trust Framework](gaia-x_trust_framework.md) – which is mandatory and verifies the existence and veracity of any service characteristics. The ‘Trust Framework’ subsystem enforces a common set of rules every participant and services in the Gaia-X Ecosystem must be verifiable against to be Gaia-X Compliant.

- The [Labelling Framework](labelling_framework.md) – which is optional and allows to verify adherence to rule-sets that fulfil specific market needs.

## References
- [Gaia-X Compliance Service deployment scenario](https://gaia-x.eu/news/latest-news/gaia-x-compliance-service-deployment-scenario/)
- [Gaia-X Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/)
- [Gaia-X Policy Rules & Labelling Document]


