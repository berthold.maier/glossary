# Physical Resource

A physical resource is a [resource](resource.md) that has a weight, position in space and represents physical [entity](entity.md) that hosts, manipulates, or interacts with other physical entities.

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 4.3
