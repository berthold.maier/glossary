# Data Product Usage Contract 

A legally binding agreement made by and between a [Data Provider](data_provider.md) and a [Data Consumer](data_consumer.md) specifying the terms and conditions of a data exchange.

The Data Product Usage Contract is a Ricardian contract: a contract at law that is both human-readable and machine-readable, cryptographically signed and rendered tamper-proof, verifiable in a decentralized fashion, and electronically linked to the subject of the contract, i.e., the data. The parties can (optionally) request this contract to be notarized in a federated Data Product Usage Contract Store.

## References
- [GXFSv2 Data Exchange](https://gaia-x.gitlab.io/technical-committee/federation-services/data-exchange/) 22.10, 2.2 

## alias
- Data Contract
