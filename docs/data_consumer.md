# Data Consumer

A participant that receives data in the form of a [Data Product](data_product.md). The data is used for query, analysis, reporting or any other data processing.

## References
- [GXFSv2 Data Exchange](https://gaia-x.gitlab.io/technical-committee/federation-services/data-exchange/) 22.10, 2.2 
