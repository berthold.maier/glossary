# Gaia-X Data Spaces Business Committee (DSBC)

The Data Space Business Committee is driven by a business ecosystem and strives to proactively support the creation of [Data Spaces](data_space.md) by third parties across Europe and beyond.
It supports the link with the [Lighthouse Projects](lighthouse_projects), the [Gaia-X Hubs](gaia-x_hub.md) and the [Vertical Ecosystems](vertical_ecosystems.md).

## References
https://gaia-x.eu/who-we-are/association/
