# Wallet

A wallet enables to store, manage, and present [Verifiable Credentials](verifiable_credential.md).

## References
- [Gaia-X Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/) 22.10 - 6.4

## alias
- Verifiable Credential Wallet
