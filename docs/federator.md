# Federator

Federators are  Gaia-X [Participants](participant.md) in charge of the [Federation Services](federation_services.md) and the [Federation](federation.md) which are independent of each other.

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 4.1.2

