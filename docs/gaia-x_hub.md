# Gaia-X Hub

Gaia-X Hubs are the central contact points for interested parties in each country. They are not a body of the Association, but they may be viewed as think tanks and grass root supporters for the Gaia-X project.

## References
https://gaia-x.eu/wp-content/uploads/2022/11/Gaia-X_TwoPagerHubs_A4_V05_01112022.pdf

