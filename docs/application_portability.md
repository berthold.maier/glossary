# Application portability

It refers to the porting of customer or third party executable code from one cloud service to another, public or private.

## References
- [Gaia-X Trust Framework](gaia-x_trust_framework.md) 22.10 - 6.7.3 Application Portability
- [ISO/IEC 19941:2017](https://www.iso.org/standard/66639.html) 5.2.3
