# Switchability

This refers to moving a customer’s account from one service provider to another, including the change of contracts and necessary technical changes.

## References
- [Gaia-X Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/) 22.10 - 6.7.4
