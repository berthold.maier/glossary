# Shape (in the context of Self-Descriptions)

Self-Descriptions are Verifiable Presentations serialized in JSON-LD. Inside these, there are graph-structured claims about the "credential subject". These credential subjects need to be validated against shapes.Shapes for validating graphs are implemented in [SHACL](https://www.w3.org/TR/shacl/). While shapes define validation rules, ontologies define terms and relationships between them, in other words the vocabulary that's used to build claims from.

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10
