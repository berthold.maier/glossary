# Presentation

Data derived from one or more [verifiable credentials](verifiable_credential.md), issued by one or more [issuers](issuer.md), that is shared with a specific [verifier](verifier.md).

## References
- [W3C, Verifiable Credentials Data Model v1.1](https://www.w3.org/TR/vc-data-model/#dfn-presentations)
