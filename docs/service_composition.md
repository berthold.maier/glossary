# Service Composition

Service Composition is the ability to realize aggregated [Service Offerings](service_offering.md).

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 4.2, 4.7
