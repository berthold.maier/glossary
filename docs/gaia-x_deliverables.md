# Gaia-X Deliverables

Gaia-X delivers:
- Specifications
- Open-Source Software
- Labels

## References
- [Gaia-X Framework](https://docs.gaia-x.eu/framework/)
