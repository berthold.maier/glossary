# Sovereignty

Sovereignty is the ability to exercise self-determination. It can translate into several meanings- political, economic, digital, and
technical. [Gaia-X](gaia-x.md) does not provide any political or economic interpretation of sovereignty, but instead provides a framework to
configure sovereignty from a digital and technical perspective.

## References
- [Gaia-X Vision & Strategy](https://gaia-x.eu/wp-content/uploads/2021/12/Vision-Strategy.pdf) - 1.3
