# Label Level 3

With reference to the three [Gaia-X Basic Labels](gaia-x_basic_labels.md): Label Level 3 targets the highest standards for data protection, security, transparency, portability, and flexibility, as well as European control. It extends the requirements of Levels 1 and 2, with criteria that ensure immunity to non-European access and a strong degree of control over vendor lock-in. A service location in Europe is mandatory. For cybersecurity, the minimum requirement will be to meet ENISA’s European Cybersecurity Scheme - High Level. 

# References
- [Gaia-X Labelling Framework](https://gaia-x.eu/wp-content/uploads/files/2021-11/Gaia-X%20Labelling%20Framework_0.pdf)
