# Metadata 

Data about other [data](data.md), documents, or set of data that describes their content, context, structure, data format, provenance, and/or rights attached to them.

## References
- [GXFSv2 Data Exchange](https://gaia-x.gitlab.io/technical-committee/federation-services/data-exchange/) 22.10, 2.2 
