# Gaia-X Policy Rules and Labelling Document

The Gaia-X Policy Rules and Labelling Document is a [Gaia-X deliverable](gaia-x_deliverables.md) resulting from the merging of the Gaia-X Policy Rules and the Gaia-X Labelling Criteria documents. The Policy Rules define high-level objectives safeguarding the added value and principles of the [Gaia-X Ecosystem](gaia-x_ecosystem.md). To allow for validation, the high-level objectives are underpinned by the Gaia-X Labelling Criteria (part of the document) and the [Gaia-X Trust Framework](gaia-x_trust_framework.md).

# References
- [Gaia-X Framework](https://docs.gaia-x.eu/framework/)
- [Gaia-X Policy Rules and Labelling Document 22.11](https://docs.gaia-x.eu/policy-rules-committee/policy-rules-labelling/22.11/) - Preface

## alias
- Policy Rules and Labelling Document

