# Principal

In the context of Identity and Access Management, it is the digital representation of a natural person, acting on behalf of a [Participant](participant.md).

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 7.5
