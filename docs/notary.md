# Notary

Entity recognized by Gaia-X that translates [Trusted Data Source](trusted_data_source.md) on behalf of the entity managing the Trusted Data Source into [Verifiable Credentials](verifiable_credential.md)

## References
- [Gaia-X Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/) 22.10 - 4 

