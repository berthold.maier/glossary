# Data Provider

A [participant](participant.md) that acquires the right to access and use some data and that makes [Data Products](data_product.md) available.

## References
- [GXFSv2 Data Exchange](https://gaia-x.gitlab.io/technical-committee/federation-services/data-exchange/) 22.10, 2.2 

