# Instantiated Virtual Resource

An Instantiated Virtual Resource represents an instance of a [Virtual Resource](virtual_resource.md). It is equivalent to a [Service Instance](service_instance.md) and is characterized by endpoints and access rights.

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 4.3

## alias
[Service Instance](service_instance.md)
