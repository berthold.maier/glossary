# Data Resource

Data Resource is a subclass of [virtual resource](virtual_resource.md) exposed through a [service instance](service_instance.md).

## References
- [Gaia-X Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/) 22.10 7.4
- [GXFSv2 Data Exchange](https://gaia-x.gitlab.io/technical-committee/federation-services/data-exchange/) 22.10, 2.2 

## alias
[Data Product](data_product.md) 
