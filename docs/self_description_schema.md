# Self-Description Schema

Collection of class'[data schema](https://www.w3.org/TR/vc-data-model/#data-schemas) describing Gaia-X [entities](entity.md).
In Gaia-X, we use the term "schema" to refer to [shapes](#shape.md), as well as ontology, as well as controlled vocabularies.
While shapes define validation rules, ontologies define terms and relationships between them, in other words the vocabulary that's used to build claims from. Controlled vocabularies are a part of that vocabulary that's more flat and extensive than the formally more complex part called "ontology": big sets of standard references, country codes, etc.

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 5.2
