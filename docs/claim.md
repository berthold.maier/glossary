# Claim

An assertion about [Entities](entity.md) expressed in the RDF data model.

## References
- [Gaia-X Architecture Document](gaia-x_architecture_document.md) 22.10 - 5.1
- [W3C, Verifiable Credentials Data Model v1.1](https://www.w3.org/TR/vc-data-model/#claims)
