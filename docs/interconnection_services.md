# Interconnection Services

Interconnection Services ensure secure and performant data exchange between the different [Providers](provider.md), [Consumers](consumer.md) and their [services](service_offering.md). Gaia-X enables combinations of services that range across multiple Providers of the [ecosystem](ecosystem.md). The Interconnection Services are also the key enabler for the composition of services offered by diverse and distributed providers, ensuring the performance of single-provider networks on a multi-provider "composed" network.

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 7.1.3
- [Interconnection Whitepaper](https://gaia-x.eu/wp-content/uploads/2022/05/The-role-and-the-importance-of-Interconnection-in-Gaia-X_13042022.pdf)
