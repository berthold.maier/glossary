# Resource

Resources describe in general the goods and objects of the [Gaia-X Ecosystem](gaia-x_ecosystem.md). A Resource can be a:

- [Physical Resource](physical_resource.md): it has a weight, position in space and represents physical entity that hosts,
manipulates, or interacts with other physical entities.
- [Virtual Resource](virtual_resource.md): it represents static data in any form and necessary information such as dataset,
configuration file, license, keypair, an AI model, neural network weights, etc.
- [Instantiated Virtual Resource](instantiated_virtual_resource.md): it represents an instance of a Virtual Resource. It is equivalent to a Service
Instance and is characterized by endpoints and access rights.

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 4.3
