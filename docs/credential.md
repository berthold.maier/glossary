# Credential

A set of one or more [claims](claim.md) made by an [issuer](issuer.md).

## References
- [W3C, Verifiable Credentials Data Model v1.1](https://www.w3.org/TR/vc-data-model/#dfn-credential)
