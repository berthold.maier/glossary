# Service Access Point

A service access point is an identifying label for network endpoints used in the [OSI model](https://en.wikipedia.org/wiki/OSI_model).

## References
- [Gaia-X Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/) 22.10 - 6.2 
