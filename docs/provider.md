# Provider

A Provider is a [Participant](participant.md) who operates [Resources](resource.md) in the [Gaia-X Ecosystem](gaia-x_ecosystem.md) and offers them as [services](service_offering.md). 

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 4.1.1


