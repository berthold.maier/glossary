# Gaia-X Trust Framework

The Gaia-X Trust Framework is a [Gaia-X deliverable](gaia-x_deliverables.md) containing the set of rules that define the minimum baseline to be part of the [Gaia-X Ecosystem](gaia-x_ecosystem.md). 

## References
- [Gaia-X Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/)
- [Gaia-X Framework](https://docs.gaia-x.eu/framework/)

## alias
- Trust Framework
