# End-User

End-Users use service offerings of a [Gaia-X Consumer](consumer.md) that are enabled by Gaia-X. The End-User uses the [Service Instances](service_instances) containing [Self-Descriptions](self_description.md) and [Policies](policy.md). 

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 4.9.1



