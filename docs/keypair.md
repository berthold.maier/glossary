# Keypair

A keypair is defined as a tuple of corresponding asymmetric encryption public and private key and can be revoked or active.

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 5.4
