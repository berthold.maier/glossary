# Verifiable Presentation

A verifiable presentation is a tamper-evident [presentation](presentation.md) encoded in such a way that authorship of the [data](data.md) can be trusted after a process of cryptographic verification.

## References
- [W3C, Verifiable Credentials Data Model v1.1](https://www.w3.org/TR/vc-data-model/#presentations)
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 5.1
