# GXDCH (Gaia-X Digital Clearing House)

The GXDCH is the necessary element to operationalize [Gaia-X](gaia-x.md) in the market. The [Gaia-X Framework](#gaia-x_framework.md) describes functional specifications, technical requirements, and SW assets necessary to be Gaia-X compliant.
The GXDCH are a network of execution nodes for the compliance components that we have developed. This safeguards the distributed, decentralised ways of running the Gaia-X compliance, not operated centrally by the Association, and where anybody can benefit from the open, transparent, and secure federated digital ecosystem – thus making the Gaia-X mission a reality.

## References
- https://gaia-x.eu/gxdch/
