# Federation

A Federation refers to a loose set of interacting actors that directly or indirectly consume, produce, or provide related [Resources](resource.md).

## References
- [Gaia-X Architecture Document](https://gaia-x.gitlab.io/technical-committee/architecture-document/) 22.10 - 4.1.2
