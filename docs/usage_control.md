# Usage Control

It is about the specification and enforcement of restrictions regulating what must (not) happen to [data](data.md). Thus, usage control is concerned with requirements that pertain to data processing (obligations), rather than data access (provisions). 
In contrast to access control, the overall goal of usage control is to enforce usage restrictions for data after
access has been granted. Therefore, the purpose of usage control is to bind policies to data being exchanged.

## References
- [GXFSv2 Data Exchange](https://gaia-x.gitlab.io/technical-committee/federation-services/data-exchange/) 22.10 - 2.7
