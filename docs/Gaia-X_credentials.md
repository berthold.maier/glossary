# Gaia-X Credentials

Gaia-X credentials - formely known as Self-Descriptions (_SD_) - are cryptographically signed attestation describing [Entities](entity.md)  from the Gaia-X Conceptual Model in a machine interpretable format.
Gaia-X credentials are [W3C Verifiable Credentials](https://www.w3.org/TR/vc-data-model/#credentials) with claims expressed in [RDF](https://www.w3.org/TR/rdf11-primer/).

## References
- [Gaia-X Architecture Document](architecture_document.md) 23.0X- draft


