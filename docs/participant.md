# Participant

A Participant is a Legal or Natural Person, which is identified, onboarded and has a [Gaia-X Self-Description](self_description.md). A Participant can take on one or more of the following roles: [Provider](provider.md), [Consumer](consumer.md), [Federator](federator.md).

## References
- [Gaia-X Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/) 22.10 - 5
- [Gaia-X Architecture Document 22.10](https://gaia-x.gitlab.io/technical-committee/architecture-document/) - 4.1



