# Data Product Self-Description 

The [Self-Description](self_description.md) of a [Data Product](data_product.md). Data Product Self-Description contains a [Data License](data_license.md). It constitutes a data usage contract template.

## References
- [GXFSv2 Data Exchange](https://gaia-x.gitlab.io/technical-committee/federation-services/data-exchange/) 22.10, 2.2 
