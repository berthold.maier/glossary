# Gaia-X Glossary

The Glossary includes a list of terms used by Gaia-X, together with their definition. 

The terms derive mainly from Gaia-X deliverables and documents. Where relevant, also terms from external sources are reported.

The output is published here https://gaia-x.gitlab.io/glossary.
